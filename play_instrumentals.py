import spotipy
import os
from spotipy.oauth2 import SpotifyClientCredentials
from time import sleep
import numpy as np


def normalize(x,N):
    # Normalize x to N
    return (x-min(x))/(max(x) - min(x)) * N


# Standard terminal is 80 characters wide
def print_status(current_progress,finished,width=80):
    
    prog = int((current_progress / finished)*100)
    print_str = ''
    for i in range(prog):
        print_str+='#'
    print(print_str+f" {prog}%")


def get_this_id_and_uri(so, artist, track):
    found = False
    query = f"track:{track} artist:{artist}"
    tries = 0
    while not found:
        try:
            ret = so.search(query)
            if ret:
                found = True
            else:
                print("No response, sleeping.")
                sleep(1)
        except Exception as e:
            tries += 1
            print(f"Got {e}.  Attempt {tries}/10")
            sleep(3)
        if tries == 9:
            token = spotipy.util.prompt_for_user_token(username, scope)
            
        if tries == 10:
            found = True
            return None, None
    try:
        this_id = ret["tracks"]["items"][0]["id"]
        uri = ret["tracks"]["items"][0]["uri"]
    except:
        this_id = None
        uri = None
    return this_id, uri

def do_shit():
    try:
        for i,l in enumerate(lines):
            if i in update_markers:
                print_status(i,len(lines))
            # Black Country, New Road and Tyler, The Creator fuck things up
            try:
                this_song = l.split(',')[0]
                artist = this_song.split(':')[0]
                track = this_song.split(':')[1]
            except IndexError:
                this_song = l.split(',')[0] +',' + l.split(',')[1]
                artist = this_song.split(':')[0]
                track = this_song.split(':')[1]
            if this_song.strip() not in played:
                this_id, uri = get_this_id_and_uri(obj, artist, track)
                sleep(0.1)
                if this_id:
                    print(f"Playing {this_song.strip()}")
                    try:
                        obj.start_playback(uris=[uri])
                    except:
                        print("Got timeout, trying again.")
                        sleep(3)
                        obj.start_playback(uris=[uri])
                    sleep(5)
                    songlog.write(f'{this_song.strip()}\n')
            else:
                print(f"Already played {this_song.strip()}")
        songlog.close()
    except Exception as error:
        if "NO_ACTIVE_DEVICE" in error:
            do_shit()
        else:
            print("Got error: ", error)
            songlog.close()
            exit(0)
cid = "LMAO"
secret = "GOTTEM"


ccm = SpotifyClientCredentials(client_id=cid, client_secret=secret)
sp = spotipy.Spotify(client_credentials_manager=ccm)

os.environ["SPOTIPY_CLIENT_ID"] = cid
os.environ["SPOTIPY_CLIENT_SECRET"] = secret
os.environ["SPOTIPY_REDIRECT_URI"] = "https://localhost:8888/callback"

scope = "user-library-read"
scope = "user-modify-playback-state"
username = "TOTESMAGOATS"
token = spotipy.util.prompt_for_user_token(username, scope)

with open("instrumental_songs.txt", "r") as infile:
    lines = infile.readlines()
infile.close()
if token:
    obj = spotipy.Spotify(auth=token)
else:
    print("Token's broken")
    exit(0)
errors = 0
songlog = open('played_songs.txt','r')
played = [l.strip() for l in songlog.readlines()]
songlog.close()
songlog = open('played_songs.txt','a')
update_markers = [int(b) for b in normalize(np.array([x for x in range(80)]), len(lines))]
do_shit()
