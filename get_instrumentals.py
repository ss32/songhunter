import os

import spotipy
from spotipy.oauth2 import SpotifyClientCredentials
import time
import ast
from typing import List
from os import listdir
from time import sleep
import tqdm
import pdb

def get_features(obj, this_id):
    found = False
    tries = 0
    while not found:
        try:
            features = obj.audio_features(this_id)
            if features:
                found = True
            else:
                print("No response, sleeping.")
                sleep(1)
        except Exception as e:
            tries += 1
            print(f"Got {e}.  Attempt {tries}/10")
            sleep(3)
        if tries == 10:
            features = None
            found = True
    return features


def get_streamings(path: str = ".") -> List[dict]:
    files = [x for x in listdir(path) if x.split(".")[0][:-1] == "StreamingHistory"]

    all_streamings = []

    for file in files:
        with open(file, "r", encoding="UTF-8") as f:
            new_streamings = ast.literal_eval(f.read())
            all_streamings += [streaming for streaming in new_streamings]
    return all_streamings


def get_this_id_and_uri(so, artist, track):
    found = False
    query = f"track:{track} artist:{artist}"
    tries = 0
    while not found:
        try:
            ret = so.search(query)
            if ret:
                found = True
            else:
                print("No response, sleeping.")
                sleep(1)
        except Exception as e:
            tries += 1
            print(f"Got {e}.  Attempt {tries}/10")
            sleep(3)
        if tries == 10:
            found = True
            return None, None
    try:
        this_id = ret["tracks"]["items"][0]["id"]
        uri = ret["tracks"]["items"][0]["uri"]
    except:
        this_id = None
        uri = None
    return this_id, uri


streams = get_streamings()
cid = "LMAO"
secret = "GOTTEM"


ccm = SpotifyClientCredentials(client_id=cid, client_secret=secret)
sp = spotipy.Spotify(client_credentials_manager=ccm)

os.environ["SPOTIPY_CLIENT_ID"] = cid
os.environ["SPOTIPY_CLIENT_SECRET"] = secret
os.environ["SPOTIPY_REDIRECT_URI"] = "https://localhost:8888/callback"

scope = "user-library-read"
scope = "user-modify-playback-state"
username = "TOTESMAGOATS"

with open("instrumental_songs.txt", "r") as infile:
    lines = infile.readlines()
infile.close()
tracks = []
for l in lines:
    tracks.append(l.strip())

token = spotipy.util.prompt_for_user_token(username, scope)
outfile = open("instrumental_songs.txt", "a")
with open("log.txt", "r") as logfile:
    processed = [l.strip() for l in logfile.readlines()]

logfile = open("log.txt", "a")
errors = open("errors.txt", "a")
n_errors = 0
if token:
    obj = spotipy.Spotify(auth=token)
    for s in tqdm.tqdm(streams, total=len(streams)):
        artist, track = s["artistName"], s["trackName"]
        this_song = f"{artist}: {track}"
        if this_song not in tracks and this_song not in processed:
            this_id, uri = get_this_id_and_uri(obj, artist, track)
            if this_id:
                features = get_features(obj, this_id)
                if features[0]:
                        if (
                            features[0]["instrumentalness"] >= 0.5
                            or features[0]["acousticness"] >= 0.5
                        ):
                            print(f"Instrumental: {this_song}")
                            outfile.write(
                                f"{this_song},{features[0]['instrumentalness']},{features[0]['acousticness']}\n"
                            )
                else:
                    errors.write(f'No features: {this_song}')
                    print(f'No features: {this_song}')
            else:
                token = spotipy.util.prompt_for_user_token(username, scope)
                if token:
                    obj = spotipy.Spotify(auth=token)
                    errors.write("Restting token!")
                else:
                    errors.write(f"{this_song}\n")
                    n_errors+=1
            sleep(0.1)
            processed.append(this_song)
            logfile.write(f"{this_song}\n")
        else:
            print(f"Skipping {this_song}")
        if n_errors>10:
            break
    outfile.close()
    logfile.close()
else:
    print("Couldn't get token")
